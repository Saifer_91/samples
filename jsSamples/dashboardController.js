//this snipet represents part of a dashboard on realconnex.com.
//I coded the router provider with delay for animation
//Notifications for each tab
//Custom pagination of projects

var App = angular.module('app');

App.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: '..my path../allDashboard.htm',
            controller: 'DashboardController'
        }).
        when('/projects', {
            templateUrl: '..my path../projectsDash.htm',
            controller: 'ProjectsController'
        }).
        when('/tabs/:id', {
            templateUrl: '..my path../profileDashboard.htm',
            controller: 'TabsInnerController',
            resolve: {
                delay: function($q, $timeout) {
                    var delay = $q.defer();
                    $timeout(delay.resolve, 300);
                    return delay.promise;
                }
            }
        }).
        when('/tabs/:id/:tab', {
            templateUrl: '..my path../profileDashboard.htm',
            controller: 'TabsInnerController',
            resolve: {
                delay: function($q, $timeout) {
                    var delay = $q.defer();
                    $timeout(delay.resolve, 100);
                    return delay.promise;
                }
            }
        }).
        when('/myProject/:id', {
            templateUrl: '..my path../projectInner.htm',
            controller: 'ProjectInnerController',
            resolve: {
                delay: function($q, $timeout) {
                    var delay = $q.defer();
                    $timeout(delay.resolve, 300);
                    return delay.promise;
                }
            }
        }).
        when('/myProject/:id/:tab', {
            templateUrl: '..my path../projectInner.htm',
            controller: 'ProjectInnerController',
            resolve: {
                delay: function($q, $timeout) {
                    var delay = $q.defer();
                    $timeout(delay.resolve, 300);
                    return delay.promise;
                }
            }
        }).
        otherwise({
            redirectTo: '/'
        });
}]);

App.controller('DashboardController', ['$scope', '$http', function($scope, $http) {
    $scope.currentTab = '..my path../tabs/mainDashboard.htm';
    $http.post('/dashboard/allDashboard').success(function(response) {
        if (response.error === false) {

            $scope.data = response.data;

            $scope.dataRoomsActivitiesCount = 0;
            $scope.lighthouseActivitiesCount = 0;
            $scope.savedSearchActivitiesCount = 0;

            angular.forEach($scope.data, function(elem) {
                if (typeof elem.features.dataRooms !== 'undefined') {
                    $scope.dataRoomsActivitiesCount += elem.features.dataRooms;
                }

                if (typeof elem.features.lighthouse !== 'undefined') {
                    $scope.lighthouseActivitiesCount += elem.features.lighthouse;
                }

                if (typeof elem.features.savedSearch !== 'undefined') {
                    $scope.savedSearchActivitiesCount += elem.features.savedSearch;
                }
            });

            $('#companyProgress').progressbar({value: $scope.data.Company.profile_completeness});
            $('#userProgress').progressbar({value: $scope.data.Me.profile_completeness});

        }
    });

    $scope.go = function(path) {
        window.location.replace(path);
    };
}]);

App.controller('TabsController', ['$scope', function($scope) {

    if ($scope.roomsActivity) {
        $scope.dataRoomsTab = '..my path../tabs/dataRoomsActivity.htm';
    } else {
        $scope.dataRoomsTab = '..my path../tabs/noDataRoomActivity.htm';
    }

    if ($scope.activityFeed) {
        $scope.activityFeedTab = '..my path../tabs/activityFeed.htm';
    } else {
        $scope.activityFeedTab = '..my path../tabs/noFeed.htm';
    }

    $scope.currentTab = $scope.activityFeedTab;

    $scope.onClickTab = function(tab) {
        $scope.currentTab = tab;
    };

    $scope.isActiveTab = function(tabUrl) {
        return tabUrl == $scope.currentTab;
    };

}]);

App.controller('ProjectsController', ['$scope', '$http', 'projectsFactory', 'searchTemplatesProvider', function($scope, $http, projectsFactory, searchTemplatesProvider) {
    $scope.loop = 0;
    $scope.isNext = true;
    $scope.isPrev = false;

    $scope.dataRoomNotificationsCount = 0;
    $scope.lighthouseNotificationsCount = 0;
    $scope.savedSearchNotificationsCount = 0;

    $scope.projects = projectsFactory.get({limit: 4, offset: 0});

    $scope.projects.$promise.then(function() {
        angular.forEach($scope.projects, function(project) {
            $scope.dataRoomNotificationsCount += project.dataRoomNotifications;
        });
    });

    $http.get('/searchTemplates/getNotifications/' + 3).success(function(res) {
        $scope.savedSearchNotifications = res.notifications;
        $scope.savedSearchNotificationsCount = res.notificationCount;
        var newArray = [];

        angular.forEach($scope.savedSearchNotifications, function(template) {
            angular.forEach(template, function(notification) {
                newArray.push(notification);
            });
        });

        $scope.savedSearchNotifications = newArray;
    });

    $http.get('/searchTemplates/getNotifications/' + 3 + '/0/2').success(function(res) {
        $scope.lighthouseNotifications = res.notifications;
        $scope.lighthouseNotificationsCount = res.notificationCount;
        var newArray = [];

        angular.forEach($scope.lighthouseNotifications, function(template) {
            angular.forEach(template, function(notification) {
                newArray.push(notification);
            });
        });

        $scope.lighthouseNotifications = newArray;
    });

    $scope.nextProjects = function(loop) {
        $scope.isPrev = true;
        $scope.loop = loop + 4;

        var old = angular.copy($scope.projects);

        projectsFactory.get({
            limit: 4,
            offset: $scope.loop
        }).$promise.then(function(response) {
                $scope.projects = response;
                if (response.length < 4) $scope.isNext = false;
                if (response.length == 0) {
                    $scope.projects = angular.copy(old);
                    $scope.isNext = false;
                }
            });
    };

    $scope.prevProjects = function(loop) {
        $scope.isNext = true;
        $scope.loop = loop - 4;
        $scope.projects = projectsFactory.get({
            limit: 4,
            offset: $scope.loop
        });
        if ($scope.loop == 0) {
            $scope.isPrev = false;
        }
    };

    $scope.animate = function(id) {
        $('#projectBox' + id).css({position: 'relative'}).
            animate({right: '151%', width: '248px'},{duration: 200});
    };

    $scope.go = function ( path ) {
        window.location.replace(path);
    };
}]);

App.controller('ProjectInnerController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
    $scope.tabFlag = 'projects';
    $scope.activityFeedAll = [];
    $scope.currentTab = '..my path../tabs/activityFeed.htm';
    $scope.lighthouseTab = '..my path../tabs/lighthouseFeed.htm';
    $scope.savedSearchTab = '..my path../tabs/savedSearch.htm';
    $scope.dataRoomNotificationsCount = 0;
    $scope.lighthouseNotificationsCount = 0;
    $scope.savedSearchNotificationsCount = 0;

    $scope.projectTypes = {
        1: 'Fund',
        2: 'Project',
        3: 'Loan',
        4: 'Mezz',
        5: 'Service',
        6: 'Fund',
        7: 'Listing'
    };

    if ($routeParams.id) {
        $http.post('/dashboard/getSingleProject/' + $routeParams.id).success(function(response) {
            if (response.error === false) {
                $scope.projectID = response.data.data.Project.id;
                $scope.userID = response.data.userID;
                $scope.projectLogo = response.data.data.Project.logo;
                $scope.projectTitle = response.data.data.Project.title;
                $scope.projCompleteness = response.data.completeness;
            }
        });
    }

    $http.post('/dashboard/dataRoomActivity/projects/' + $routeParams.id).success(function(response) {
        if (response.error === false) {
            $scope.roomsActivity = response.data;
            $scope.dataRoomNotificationsCount   = response.dataRoomNotificationsCount;
            $scope.activityFeed = response.activity;
            angular.forEach(response.activity, function(notification) {
                notification.DataRoomActivity['User'] = notification.User;
                $scope.activityFeedAll = $scope.activityFeedAll.concat(notification.DataRoomActivity);
            });
            $scope.activityFeedTab = (response.activity !== null && response.activity.length > 0 || $scope.lighthouseNotifications !== null) ?
                '..my path../tabs/activityFeed.htm' : '..my path../tabs/noFeed.htm';

            $scope.dataRoomsTab = (Object.keys(response.data).length > 0) ?
                '..my path../tabs/dataRoomsActivity.htm' : '..my path../tabs/noDataRoomActivity.htm';

            if ($routeParams.tab) {
                switch ($routeParams.tab) {
                    case 'dataRooms':
                        $scope.currentTab = $scope.dataRoomsTab;
                        break;

                    case 'lighthouse':
                        $scope.currentTab = $scope.lighthouseTab;
                        break;

                    case 'savedSearch':
                        $scope.currentTab = $scope.savedSearchTab;
                        break;

                    default:
                        $scope.currentTab = $scope.activityFeedTab;
                        break;
                }
            }


        }
    });

    $scope.loadSavedSearchNotifications = function(limited, isActivity) {
        var entityId = 3;
        limited = limited ? ('?limited=' + limited) : '';

        $http.get('/searchTemplates/getNotifications/' + entityId + '/' + $routeParams.id + limited).success(function(res) {
            if (isActivity) {
                angular.forEach(res.notifications, function(notification) {
                    $scope.activityFeedAll = $scope.activityFeedAll.concat(notification);
                });
            }
            $scope.savedSearchNotifications = res.notifications;
            $scope.savedSearchNotificationsCount = res.notificationCount;
        });
    };

    $scope.loadLighthouseNotifications = function(limited, isActivity) {
        var entityId = 3;
        limited = limited ? ('?limited=' + limited) : '';

        $http.get('/searchTemplates/getNotifications/' + entityId + '/' + $routeParams.id + '/2' + limited).success(function(res) {
            if (isActivity) {
                angular.forEach(res.notifications, function(notification) {
                    $scope.activityFeedAll = $scope.activityFeedAll.concat(notification);
                });
            }
            $scope.lighthouseNotifications = res.notifications;
            $scope.lighthouseNotificationsCount = res.notificationCount;
        });
    };

    $scope.loadSavedSearchNotifications(false, true);
    $scope.loadLighthouseNotifications(false, true);

    $scope.clearLighthouseNotifications = function() {
        var entityId = $routeParams.id == 'me' ? 1 : ($routeParams.id == 'company' ? 2 : 3);
        $http.get('/searchTemplates/clearNotifications/' + entityId + '/2');
        $scope.lighthouseNotifications = [];
        $scope.lighthouseNotificationsCount = 0;
    };

    $scope.clearSavedSearchNotifications = function() {
        var entityId = $routeParams.id == 'me' ? 1 : ($routeParams.id == 'company' ? 2 : 3);
        $http.get('/searchTemplates/clearNotifications/' + entityId + '/1');
        $scope.savedSearchNotifications = [];
        $scope.savedSearchNotificationsCount = 0;
    };

    $scope.clearNotification = function(notification, notifications, template_id) {
        $http.get('/searchTemplates/clearNotification/' + notification.id);
        notifications.splice(notifications.indexOf(notification), 1);
        $scope.lighthouseNotificationsCount--;
        if (notifications.length < 1) {
            delete $scope.lighthouseNotifications[template_id];
            if (angular.equals({}, $scope.lighthouseNotifications)) {
                $scope.lighthouseNotifications = [];
            }
        }
    };

    $scope.goToDataRoom = function(path, id) {
        $http.get('/dashboard/markActivityAsViewed/' + id).success(function() {
            window.location.href = '/collaboration-rooms#!' + path;
        });
    };

}]);

