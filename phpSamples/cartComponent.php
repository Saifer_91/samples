<?php
//This snippet represents method of adding products in to cart on https://www.endlesslyorganic.com/
/**
 * Created by PhpStorm.
 * User: john
 * Date: 21.10.14
 * Time: 15:01
 */
class ProductsController extends AppController
{
    public $name = "Products";

    public $components = array("Cart", "AuthorizeNet", "Messenger", "RequestHandler", 'Password');

    public $uses = array("Product", "Category", "Coupon", "State", 'HearSource');

    public $helpers = array('Number', 'Text');

    public $paginate = array(
        'limit' => 9,
        'order' => 'sort',
        'recursive' => -1,
        'conditions' => array(
            'Product.active' => true
        )
    );

    public function cart($id = null, $quantity = 1)
    {
        $this->layout = 'full_width';

        if ((!is_null($id) && is_numeric($id)) || !empty($this->data)) {
            if (!empty($this->data['Product']['id']) && (is_null($id) || !is_numeric($id))) {
                $id = $this->data['Product']['id'];
                $quantity = !empty($this->data['Product']['quantity']) ? $this->data['Product']['quantity'] : $quantity;
                $options = !empty($this->data['Product']['options']) ? $this->data['Product']['options'] : array();
                if (isset($this->data['Product']['is_subscription']) && in_array($this->data['Product']['is_subscription'], array(0, 1))) {
                    $options['is_subscription'] = $this->data['Product']['is_subscription'];
                }
                $this->Cart->addProduct($id, $quantity, $options);
            } elseif (!empty($this->data['Product']) && empty($this->data['Product']['id'])) {
                foreach ($this->data['Product'] as $id => $attributes) {
                    foreach ($attributes as $key => $value) {
                        $this->Cart->setQuantity("$id.$key", $value["quantity"]);
                    }
                }
            } elseif (!empty($id) && !empty($quantity)) {
                $this->Cart->addProduct($id, $quantity);
            }
        }
        ($this->data) ? $this->redirect('/products/cart') : null;
        $locations = ClassRegistry::init('UserLocation')->getAllActive();
        $selectedLocation = $this->Session->read('Cart.LocationId');
        $this->set(compact('locations', 'selectedLocation'));
    }
}